#!/bin/bash
apt-get update
apt-get install curl -y
apt-get install tar -y
apt-get install unzip -y
apt-get install sudo -y

curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
mv /tmp/eksctl /usr/local/bin
eksctl version
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws --version
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
curl -o kubectl https://s3.us-west-2.amazonaws.com/amazon-eks/1.22.6/2022-03-09/bin/linux/amd64/kubectl
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
kubectl version --short --client

eksctl delete cluster -f cluster.yml

#eksctl create cluster -f cluster.yml
#eksctl create nodegroup -f node_group.yml

# kubectl create namespace argocd
# kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
# kubectl get pods -n argocd
# sleep 60
# kubectl get pods -n argocd
# kubectl get all -n argocd
# kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
# kubectl -n argocd patch svc argocd-server -p '{"spec": {"type": "LoadBalancer"}}'
# sleep 60
# kubectl -n argocd get svc argocd-server
#ARGO_URL=$(kubectl -n argocd get svc argocd-server --template "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")
#echo ARGO DASHBOARD: http://${ARGO_URL}:2746
#kubectl apply -f application.yaml

